#!/bin/bash

# 检查操作系统
OS=$(uname -s)
echo "当前操作系统为: $OS"

# 检查是否已安装 curl、git 和 lsof
if ! command -v curl &> /dev/null; then
    echo "curl 未安装，正在安装..."
    if [ "$OS" = "Linux" ]; then
        apt-get update
        apt-get install -y curl
    elif [ "$OS" = "Darwin" ]; then
        brew install curl
    fi
fi

if ! command -v git &> /dev/null; then
    echo "git 未安装，正在安装..."
    if [ "$OS" = "Linux" ]; then
        apt-get install -y git
    elif [ "$OS" = "Darwin" ]; then
        brew install git
    fi
fi

if ! command -v lsof &> /dev/null; then
    echo "lsof 未安装，正在安装..."
    if [ "$OS" = "Linux" ]; then
        apt-get install -y lsof
    elif [ "$OS" = "Darwin" ]; then
        brew install lsof
    fi
fi

# 检查是否已安装 Node.js
if ! command -v node &> /dev/null; then
    echo "Node.js 未安装，正在安装..."
    # 安装 nvm
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.38.0/install.sh | bash
    # source nvm
    source ~/.bashrc
    # 安装 Node.js
    nvm install 16.14.0
else
    echo "Node.js 已安装，跳过安装步骤。"
fi

# 显示 Node.js 版本
node -v

# 显示 Git 版本
git --version

# 检查端口是否被占用
check_port() {
    local port=$1
    if lsof -Pi :$port -sTCP:LISTEN -t >/dev/null; then
        echo "端口 $port 已被占用"
        return 1
    else
        echo "端口 $port 未被占用"
        return 0
    fi
}

# 停止其他 Node.js 进程
stop_other_node_processes() {
    if [ "$OS" = "Linux" ]; then
        echo "正在停止其他 Node.js 进程..."
        killall node
    elif [ "$OS" = "Darwin" ]; then
        echo "正在停止其他 Node.js 进程..."
        pkill node
    fi
}

# 停止已运行的 PM2 进程
stop_pm2() {
    if [ "$OS" = "Linux" ]; then
        echo "正在停止 PM2 进程..."
        pm2 stop all
    elif [ "$OS" = "Darwin" ]; then
        echo "正在停止 PM2 进程..."
        pm2 stop all
    fi
}

# 检查端口 4000 是否被占用
if check_port 4000; then
    echo "端口 4000 未被占用，继续执行..."
else
    echo "端口 4000 被占用，停止其他进程..."
    stop_other_node_processes
fi

# 停止已运行的 PM2 进程
stop_pm2

# 安装本体
echo "安装本体..."
git clone https://gitee.com/yuanpluss/simple-free-ai.git

# 切换目录
cd simple-free-ai

# 启动服务（使用 pm2）
echo "启动服务（使用 pm2）..."
npm run start-pm2

# 或者直接启动
# echo "启动服务..."
# npm start