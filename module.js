const path = require('path');

const dependencies = {
  FreeStableDiffusion_1: './utils/FreeStableDiffusion/SD_1',
  FreeDalle_1: './utils/Dall-e-3/dalle_1',
  handleGLM4Completions: './components/GLM4',
  handleGLM4VCompletions: './components/GLM4V',
  handleTTSCompletions: './components/TTS',
  handleChat40Completions: './components/Chat40',
  handleChat35Completions: './components/Chat35',
  handleGeminiCompletions: './components/Gemini',
  handleClaudeCompletions: './components/Claude',
  handleSearchCompletions: './components/Search',
  handleMoonshotCompletions: './components/Moonshot',
  handleSDCompletions: './components/SD',
  handleDalleCompletions: './components/dalle',
  chatglm4v: './utils/ChatGLM4/GLM4V',
  chatglm4: './utils/ChatGLM4/GLM4',
  FreeChat35_1: './utils/FreeChat35/chat35_1',
  FreeChat35_2: './utils/FreeChat35/chat35_2',
  FreeChat35_3: './utils/FreeChat35/chat35_3',
  FreeChat35_4: './utils/FreeChat35/chat35_4',
  FreeChat35_5: './utils/FreeChat35/chat35_5',
  FreeChat35_6: './utils/FreeChat35/chat35_6',
  FreeGemini_1: './utils/FreeGemini/Gemini_1',
  FreeGemini_2: './utils/FreeGemini/Gemini_2',
  FreeGemini_3: './utils/FreeGemini/Gemini_3',
  FreeClaude_1: './utils/FreeClaude/Claude_1',
  FreeSearch_1: './utils/FreeSearch/Search_1',
  FreeErnie_1: './utils/FreeErnie/Ernie_1',
  FreeKimi_1: './utils/FreeMoonshot/Kimi_1',
  FreeChat40_1: './utils/FreeChat40/chat_1',
  FreeChat40_2: './utils/FreeChat40/chat_2',
  FreeChat40_3: './utils/FreeChat40/chat_3',
  FreeChat40_4: './utils/FreeChat40/chat_4',
  FreeChat40_5: './utils/FreeChat40/chat_5',
  getAudioFromTPS_1: './utils/TTS/tts_1'
};

const exportedDependencies = {};
for (const key in dependencies) {
  exportedDependencies[key] = require(path.join(__dirname, dependencies[key]));
}

module.exports = exportedDependencies;