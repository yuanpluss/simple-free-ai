# Simple free AI for everyone

## 介绍：
简单的Ai使用演示，集成ChatGPT + Live2D + TTs，个人部署请参考下方的简单教程，开箱即用
**[demo演示](https://y-tian-plugin.top:4000/chat)**

## 安装教程：

### 1. 对于Windows用户

#### (1). 安装Node环境：

下载地址 http://nodejs.cn/download/ 
使用稳定版安装，一路Next即可

<details> <summary>NodeJS</summary>
<img src="./demonstrate/image/1.jpg" style="zoom:50%;" />
</details>

#### (2). 如果未安装git
下载地址 https://pc.qq.com/detail/13/detail_22693.html

#### (3). 配置好NodeJS环境后，运行git(右键git bash here)，输入以下指令:

* 安装本体
```
git clone https://gitee.com/yuanpluss/simple-free-ai.git

```
* 切换目录
```
cd simple-free-ai

```
* 启动服务(pm2启动)
```
npm install -g pm2 && npm run start-pm2

```

* 直接启动
```
npm run start

```

### 2. 对于Linux用户

* 如果你是 Ubuntu/Debian
```
apt update && apt install sudo && sudo apt install git && sudo apt-get install dos2unix && npm install -g pm2

```
* 如果你是 CentOS
```
yum update && yum install sudo && sudo yum install git && sudo yum install dos2unix && npm install -g pm2

```
* 一键安装
```
git clone https://gitee.com/yuanpluss/fast-install.git && cd fast-install && dos2unix install.sh && chmod +x install.sh && ./install.sh

```

### 3. 常用指令:

* 启动服务
```
npm run start/npm run start-pm2

```
* 关闭服务
```
npm run stop/npm run stop-pm2

```
* 更新/强制更新
```
npm run updates/npm run updates-f

```


**点击跳转对话[-> Chat-Ai <-](http://127.0.0.1:4000/chat)页面**
<details> <summary>效果图</summary>
<img src="./demonstrate/image/2.png" style="zoom:50%;" />
</details>